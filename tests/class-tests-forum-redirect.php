<?php
/**
 * Core unit test
 *
 * @package     ForumRedirect\Tests\Core
 * @since       3.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Core unit tests
 *
 * @since       3.0.0
 */
class Tests_Forum_Redirect extends WP_UnitTestCase {


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       3.0.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = Forum_Redirect();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod
		parent::tearDown();
	}


	/**
	 * Test the Forum_Redirect instance
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function test_forum_redirect_instance() {
		$this->assertClassHasStaticAttribute( 'instance', 'Forum_Redirect' );
	}


	/**
	 * Test constants
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function test_constants() {
		// Make SURE I updated the version.
		$this->assertSame( FORUM_REDIRECT_VER, '1.0.0' );

		// Plugin folder URL.
		$path = str_replace( 'tests/', '', plugin_dir_url( __FILE__ ) );
		$this->assertSame( FORUM_REDIRECT_URL, $path );

		// Plugin folder path.
		$path           = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$path           = substr( $path, 0, -1 );
		$forum_redirect = substr( FORUM_REDIRECT_DIR, 0, -1 );
		$this->assertSame( $forum_redirect, $path );
	}


	/**
	 * Test includes
	 *
	 * @access      public
	 * @since       3.0.0
	 * @return      void
	 */
	public function test_includes() {
		$this->assertFileExists( FORUM_REDIRECT_DIR . 'class-forum-redirect.php' );

		$this->assertFileExists( FORUM_REDIRECT_DIR . 'includes/filters.php' );
		$this->assertFileExists( FORUM_REDIRECT_DIR . 'includes/admin/meta-boxes.php' );
	}
}
