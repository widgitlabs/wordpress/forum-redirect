# Forum Redirect

## Welcome to our GitLab Repository

Allows you to override the default behavior of bbPress forums, linking them to an external site. Forum Redirect requires no real configuration... it simply adds a metabox to the forum edit screen allowing you to specify an override URL.

### Installation

1. You can clone the GitLab repository: `https://gitlab.com/widgitlabs/wordpress/bbPress-Redirect.git`
2. Or download it directly as a ZIP file: `https://gitlab.com/widgitlabs/wordpress/bbPress-Redirect/-/archive/master/bbPress-Redirect-master.zip`

This will download the latest developer copy of Forum Redirect.

### Bugs

If you find an issue, let us know [here](https://gitlab.com/evertiro/bbPress-Redirect/issues?state=open)!

### Contributions

Anyone is welcome to contribute to Forum Redirect. Please read the [guidelines for contributing](https://gitlab.com/evertiro/bbPress-Redirect/-/blob/master/CONTRIBUTING.md) to this repository.
