<?php
/**
 * Plugin Name:     Forum Redirect
 * Plugin URI:      http://wordpress.org/plugins/forum-redirect/
 * Description:     Allows you to override the default behavior of bbPress forums, linking them to an external site
 * Author:          Widgit Team
 * Author URI:      https://widgit.io
 * Version:         1.0.0
 * Text Domain:     forum-redirect
 * Domain Path:     languages
 *
 * @package         bbPress\ForumRedirect
 * @author          Daniel J Griffiths
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Forum_Redirect' ) ) {


	/**
	 * Main Forum_Redirect class
	 *
	 * @since       1.0.0
	 */
	final class Forum_Redirect {


		/**
		 * The one true Forum_Redirect
		 *
		 * @var         Forum_Redirect $instance The one true Forum_Redirect
		 * @since       1.0.0
		 */
		private static $instance;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      object self::$instance The one true Forum_Redirect
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Forum_Redirect ) ) {
				self::$instance = new Forum_Redirect();
				self::$instance->setup_constants();
				self::$instance->hooks();
				self::$instance->includes();
			}

			return self::$instance;
		}


		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'forum-redirect' ), '1.0.0' );
		}


		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'forum-redirect' ), '1.0.0' );
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin version.
			if ( ! defined( 'FORUM_REDIRECT_VER' ) ) {
				define( 'FORUM_REDIRECT_VER', '1.0.0' );
			}

			// Plugin folder URL.
			if ( ! defined( 'FORUM_REDIRECT_URL' ) ) {
				define( 'FORUM_REDIRECT_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin folder dir.
			if ( ! defined( 'FORUM_REDIRECT_DIR' ) ) {
				define( 'FORUM_REDIRECT_DIR', plugin_dir_path( __FILE__ ) );
			}
		}


		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.2.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}


		/**
		 * Include required files
		 *
		 * @access      private
		 * @since       1.1.0
		 * @return      void
		 */
		private function includes() {
			require_once FORUM_REDIRECT_DIR . 'includes/filters.php';

			if ( is_admin() ) {
				require_once FORUM_REDIRECT_DIR . 'includes/admin/meta-boxes.php';
			}
		}


		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for plugin language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'forum_redirect_lang_dir', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'forum-redirect' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'forum-redirect', $locale );

			// Setup paths.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/forum-redirect/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/forum-redirect/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Check global wp-content/languages/forum-redirect folder.
				load_textdomain( 'forum-redirect', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/forum-redirect/languages/ folder.
				load_textdomain( 'forum-redirect', $mofile_local );
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/forum-redirect/ folder.
				load_textdomain( 'forum-redirect', $mofile_core );
			} else {
				load_plugin_textdomain( 'forum-redirect', false, $lang_dir );
			}
		}
	}
}


/**
 * The main function responsible for returning the one true Forum_Redirect
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $forum_redirect = Forum_Redirect(); ?>
 *
 * @since       1.2.0
 * @return      Forum_Redirect The one true Forum_Redirect
 */
function forum_redirect() {
	return Forum_Redirect::instance();
}

// Get things started.
Forum_Redirect();
