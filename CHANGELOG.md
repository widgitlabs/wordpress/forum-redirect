# Changelog

## 1.2.0

* Improved: Bring code up to current standards

## 1.1.1

* Added: Sanity check on topic redirects

## 1.1.0

* Code cleanup
* Added support for topic redirects
* Added topic_redirect handler to prevent direct linking

## 1.0.1

* Code cleanup
* Rewrote meta box save function
* Added check for bbPress

## 1.0.0

* Initial release
